({
	AccInsert : function(component, event, helper) {
		var name = component.get("v.accname");
        var phone = component.get("v.accphone");
        var action = component.get("c.doInsert");
        action.setParams({'aname':name,'aphone':phone});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS"){
                var storeResponse = response.getReturnValue();
            }
	});
        $A.enqueueAction(action);
    }
})