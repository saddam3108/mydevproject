({
	createRecord : function(component, event, helper) {
		var cont = component.get("v.contact");
        if($A.util.isEmpty(contact.lastname) || $A.util.isUndefined(contact.lastname)){
            alert('Last Name is Required');
            return;
        } 
        if($A.util.isEmpty(contact.phone) || $A.util.isUndefined(contact.phone)){
            alert('phone number is Required');
            return;
        }
        
        var action = component.get("c.doSave");
        action.setparams({contact:con});
           //Setting the Callback
        action.setCallback(this,function(a){
            //get the response state
            var state = a.getState();
            
            //check if result is successfull
            if(state == "SUCCESS"){
                //Reset Form
                var newCandidate = {'sobjectType': 'contact',
                                    'LastName': '',
                                    'phone': ''                                   
                                   };
                //resetting the Values in the form
                component.set("v.con",newCandidate);
                alert('Record is Created Successfully');
            } else if(state == "ERROR"){
                alert('Error in calling server side action');
            }
        });
        
		//adds the server-side action to the queue        
        $A.enqueueAction(action);

	}
})