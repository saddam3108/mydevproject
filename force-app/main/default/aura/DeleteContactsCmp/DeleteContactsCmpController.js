({
	loadContactList  : function(component, event, helper) {
        console.log('Incontroller')
        alert('----hi----');
        helper.onLoad(component, event);
	},
    
    checboxSelected : function(component,event,helper){
        var selectRec = event.getSource().get("v.value");
        var getSelectedNumber = component.get("v.SelectedContacts");
        if(selectRec == true){
            getSelectedNumber++;
        }else {
            getSelectedNumber--;
        }
        component.set("v.SelectedContacts", getSelectedNumber);
    },
    
    selectAll : function(component,event,helper){
        var selectHeadeCheckbox = event.getSource().get("v.value");
        var getAllId = component.find("boxPack");
        if(selectHeadeCheckbox == true){
            for(var i=0; i < getAllId.length; i++){
                component.find("boxPack")[i].set("v.value", true);
                component.set("v.SelectedContacts", getAllId.length);
            }
        }else {
            for(var i=0; i < getAllId.length; i++){
                component.find("boxPack")[i].set("v.value", false);
                component.set("v.SelectedContacts",0);
            }
        }
    },
    
    deleteSelected : function(component,event,helper){
        var delId = [];
        var getAllId = component.find("boxPack");
        for(var i=0; i < getAllId.length; i++){
            if(getAllId[i].get("v.value") == true){
                delId.push(getAllId[i].get("v.text"));
            }
        }
        helper.deleteSelectedHelper(component,event,delId);
    },
})