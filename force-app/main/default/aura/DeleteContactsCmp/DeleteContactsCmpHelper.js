({
	onLoad : function(component, event) {
        alert('----hi2----');
		var action = component.get('c.fectconts');
        action.setCallback(this, function(response){
            var state = response.getState();
            alert('--------state-----'+state);
            if(state === "SUCCESS"){
                alert('-----response------'+response.getReturnValue());
                component.set("v.ListofContacts", response.getReturnValue());
                component.set('v.SelectedContacts', 0);
            }
        });
        $A.enqueueAction(action);
	},
    
    deleteSelectedHelper : function(component,event,deleteRecordsIds){
        var action = component.get("c.delcont");
        action.setParams({"lstRecordId": deleteRecordsIds});
        action.setCallback(this, function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                if(response.getReturnValue() != ''){
                    alert('The following error has occurred. while Delete record-->' + response.getReturnValue());
                }else {
                    console.log('check it--> delete successful');
                }
                this.onLoad(component, event);
            }
        });
        $A.enqueueAction(action);
    },
})