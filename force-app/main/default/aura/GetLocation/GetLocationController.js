({
    doInit : function(component, event, helper) {
        
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var latit = position.coords.latitude;
                var longit = position.coords.longitude;
                component.set("v.latit",latit);
                component.set("v.longit",longit);
                alert("latitude :"+latit+"  Longitude :"+longit);
                var oppId = component.get("v.recordId");
                var action = component.get("c.doInsert");
                action.setParams({
                    "rid" : oppId,
                    "lat": latit,
                    "longt" :longit
                });
             action.setCallback(this,function(response)
             {
                 var state = response.getState();
                 alert(state);
                 if(state== "SUCCESS")
                 {
                 }
             });
                $A.enqueueAction(action);
            });
        } 
    }
})