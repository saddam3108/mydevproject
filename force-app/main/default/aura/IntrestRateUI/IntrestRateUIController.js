({
	Rate : function(component) {
		var amt = component.find("amount").get("v.value");
        var yrs = component.find("year").get("v.value");
        var rat = component.find("rate").get("v.value");
        var intr = amt*yrs*rat/100;
        component.find("intrest").set("v.value",intr);
	}
})