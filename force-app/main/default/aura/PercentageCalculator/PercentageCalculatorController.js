({
	perCal : function(component, event, helper) {
        
		
        var salary = component.get("v.salary");
       
        var per = component.get("v.percentage");
        var output = salary*per/100;
        
        component.set("v.output", output);
	}
})