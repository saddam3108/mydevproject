({
	ConHelper : function(component, event) {
		var action = component.get("c.getcon");
        action.setParams({'cname': component.get("v.search")});
        alert('---------'+component.get("v.search"));
        action.setCallback(this, function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = [];
                storeResponse = response.getReturnValue();
                console.log(storeResponse);
                // if storeResponse size is 0 ,display no record found message on screen.
                console.log(storeResponse.length);
                if (storeResponse.length == 0) {
                    
                    component.set("v.Message", true);
                } else {
                    component.set("v.Message", false);
                }
                // set numberOfRecord attribute value with length of return value from server
                component.set("v.numberofRecords", storeResponse.length);
                // set searchResult list with return value from server.
                component.set("v.result", storeResponse);
            }
 
        });
        $A.enqueueAction(action);
	},
})