public class AccContact_Report {

    public Map<String, Map<Date, Integer>> conmap {set;get;}
    public Map<String, Integer> countmap {set;get;}
    public Integer count {set;get;}
    
    public AccContact_Report()
    {
    conmap=new Map<String, Map<Date, Integer>>();
    countmap=new Map<String, Integer>();
    count=0;
    
    countmap.put('ZTotal',0);
    
    for(Contact c:[select id, LeadSource, createdDate from Contact])
    {
        if(!conmap.containsKey(c.LeadSource))
        conmap.put(c.LeadSource, new Map<Date, Integer>());
        conmap.put('ZTotal',new Map<Date, Integer>());
        
        if(!conmap.get(c.LeadSource).containskey(Date.valueOf(c.createdDate)))
         conmap.get(c.LeadSource).put(Date.valueOf(c.createdDate), count);
        if(conmap.get(c.LeadSource).containskey(Date.valueOf(c.createdDate)))
        conmap.get(c.LeadSource).put(Date.valueOf(c.createdDate), conmap.get(c.LeadSource).get(Date.valueOf(c.createdDate))+1);
        //conmap.get('ztotal').put(Date.valueOf(c.createdDate), conmap.get(c.LeadSource).get(Date.valueOf(c.createdDate)));
        //countmap.put(c.leadsource, countmap.get(c.leadsource)+1);
    }
    }
    
}