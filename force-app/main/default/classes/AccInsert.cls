public class AccInsert {
    public Account acc {set;get;}
    public List<Account> aclist {set;get;}
    public List<Account> alist {set;get;}
    public AccInsert(){
        aclist = new List<Account>();
        acc = new Account();
    }
    public Pagereference dosave(){
        aclist = [select name from account limit 1];
        alist = new List<Account>();
        for(Account a : aclist){
            a.name = acc.name;
            system.debug('------After------->'+acc.name);
            alist.add(a);
        }
        Insert aclist;
        return null;
    }
}