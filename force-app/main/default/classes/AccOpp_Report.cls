public class AccOpp_Report 
{
    public date Startdate{set;get;}
    public date Enddate{set;get;}
    public List<String> StageNamelist { get; set; }
    public Map<String,Integer> StageMap { get; set; }
    public Map<string,Map<string,Integer>> OppCountMap { get; set; }
    public Boolean flag{set;get;}
    
    public AccOpp_Report()
    {
      flag=false;
        OppCountMap= new Map<string,Map<string,Integer>>();
        StageMap = new Map<String,Integer>();
        StageNamelist = new List<String>();
        
        Schema.DescribeFieldResult fieldResult2 = opportunity.stagename.getDescribe();
        List<Schema.PicklistEntry> ple2 = fieldResult2.getPicklistValues();
        for( Schema.PicklistEntry f : ple2)
        {
            StageNamelist.add(f.getValue());
            StageMap.put(f.getValue(),0);
        }
    }
    
    public void submit1()
    {  
        flag=true;
        for( opportunity o : [select account.name,name,stagename from opportunity where createddate>=:startdate and createddate<=:enddate] )
        {
            if(!OppCountMap.containskey(o.account.name))
            {
                OppCountMap.put(o.account.name,new Map<string,Integer>());
                for(String SN : StageNamelist)
                {    
                if(! OppCountMap.get(o.account.name).containskey(SN) )
                    OppCountMap.get(o.account.name).put(SN, 0);
                }
            }
            if( OppCountMap.get(o.account.name).containskey(o.stagename) )
            {
                OppCountMap.get(o.account.name).put(o.stagename, OppCountMap.get(o.account.name).get(o.stagename)+1);
            }
        }
    }
}