public class AccOpp_Report1_Cntrlr {

    //public date Startdate{set;get;}
    //public date Enddate{set;get;}
    //public List<String> StageNamelist { get; set; }
    //public Map<String,Integer> StageMap { get; set; }
    //public Boolean flag{set;get;}
    
    public Map<string,Map<string,Integer>> OppCountMap { get; set; }
    
    public AccOpp_Report1_Cntrlr() {
        OppCountMap = new Map<string,Map<string,Integer>>();
        
        Schema.DescribeFieldResult fieldResult2 = opportunity.stagename.getDescribe();
        List<Schema.PicklistEntry> SNpl = fieldResult2.getPicklistValues();
        
            for( opportunity o : [ select account.name,name,stagename from opportunity ] ){
                
                
                if(!OppCountMap.containskey(o.account.name)){
                OppCountMap.put(o.account.name,new Map<string,Integer>());
                
                for( Schema.PicklistEntry f : SNpl ){
                if(!OppCountMap.get(o.account.name).containskey(f.getValue())){
                OppCountMap.get(o.account.name).put(' - '+f.getValue(),0);
                }
                }    
                
                //if(OppCountMap.containskey(f.getValue())){
                //if(OppCountMap.get(f.getValue()).containskey(o.account.name))
                // OppCountMap.get(f.getValue()).put(o.account.name,OppCountMap.get(f.getValue()).get(o.account.name)+1);
                //}
                }
            }
    }
}