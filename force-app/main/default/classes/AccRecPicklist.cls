global class AccRecPicklist {
    webservice static List<wrapper> dopicklist(){
        wraplist = new List<wrapper>();
        for(Account a : [Select name from Account]){
           wrap = new wrapper();
            wrap.name = a.name;
            wraplist.add(wrap);
        }
        return wraplist;
    }
    
    global static wrapper wrap {set;get;}
    global static List<wrapper> wraplist {set;get;}
    global class wrapper{
        webservice string name {set;get;}
    }
}