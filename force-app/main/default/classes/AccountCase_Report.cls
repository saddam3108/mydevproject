public class AccountCase_Report {

    public String cs2 { get; set; }

    Public List<String>  statuslist {set;get;}
    public Map<string, Integer> statuscount {set;get;}
    public Map<String, Map<String,Integer>> casemap {set;get;}
    
    public AccountCase_Report(){
    statuslist = new List<String>();
    statuscount = new Map<String, Integer>();
    casemap = new Map<String,Map<String, Integer>>();
    
    Schema.DescribeFieldResult result = Case.status.getDescribe();
    List<schema.picklistEntry> res=result.getPicklistvalues();
        for(schema.picklistEntry p:res){
        statuslist.add(p.getvalue());
        //statuslist.add('ZTotal');
           //statuslist.sort();
           
        }
        for(case c:[select Account.name,status from case]){
        
            if(!casemap.containskey(c.account.name)){
                casemap.put(c.account.name, new Map<string, Integer>() );
                for(string s:statuslist){
                    if(!casemap.get(c.account.name).containskey(s))
                    casemap.get(c.account.name).put(s,0);
                    statuscount.put(s, 0);
                    }
                }
            if(casemap.get(c.account.name).containskey(c.status)){
            casemap.get(c.account.name).put(c.status, casemap.get(c.account.name).get(c.status)+1);
           
                statuscount.put(c.status,statuscount.get(c.status)+1);
            }
            
       }     
            
            
            for(String ls : casemap.keyset()){
                Integer i = 0;
                for(String sn : casemap.get(ls).keyset()){
                    i = i + casemap.get(ls).get(SN);
                }
                casemap.get(ls).put('yTotal', i);
                statuscount.put('ytotal',i);
            }
    }
}