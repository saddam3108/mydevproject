public class AccountClass {
    Public List<AccountWrapper> accwrap {set;get;}
    Public LisT<Contactwrapper> conwrap {set;get;}
    public string aname {set;get;}
    public string cname {set;get;}
    public List<Account> acclist {set;get;}
    public List<Contact> conlist {set;get;}
    Public List<case> caselist {set;get;}    
    
    Public void dosearch(){
        if(accwrap == null){
            accwrap = new List<AccountWrapper>();
        }else{
            accwrap.clear();
        }
        string soql = 'Select name from Account where';
        if(aname != null){
            soql = soql + ' name =: aname';
            for(Account ac : database.query(soql)){
                accwrap.add(new AccountWrapper(ac));
            }
        }
    }
    
        
    Public Pagereference selectedAccount(){
        acclist = new List<Account>();
        for(AccountWrapper acw : accwrap){
            if(acw.check == true){
                acclist.add(acw.acc);
            }
        }
        system.debug('-----Account list -------->'+acclist);
        Pagereference p = new Pagereference('/apex/contactpage');
        return p;
    }
    
    Public void dosearch1(){
        if(conwrap == null){
            conwrap = new List<ContactWrapper>();
        }else{
            conwrap.clear();
        }
        string soql = 'Select Lastname from Contact where ';
        if(cname != ''){
            soql = soql + ' lastname =: cname';
            for(Contact cc : database.query(soql)){
                conwrap.add(new ContactWrapper(cc));
            }
        }
    }
    
    Public Pagereference selectedContact(){
        conlist = new List<Contact>();
        for(ContactWrapper ccw : conwrap){
            if(ccw.check1 == true){
                conlist.add(ccw.con);
            }
        }
        system.debug('-----Account list -------->'+conlist);
        Pagereference p1 = new Pagereference('/apex/Casepage');
        return p1;
    }

    public void displaycases(){
        caselist = new List<Case>();
        for(Account a1 : acclist){
            for(Contact c1 : conlist){
                case c = new case();
                c.AccountId = a1.id;
                c.ContactId = c1.id;
                caselist.add(c);
            }
        }
    }
    
    Public Pagereference doInsert(){
        insert caselist;
        Pagereference p2 = new Pagereference('https://hussain123-dev-ed.my.salesforce.com/500/o');
        return p2;
    }
    
    
    public class Accountwrapper{
        Public Account acc {set;get;}
        public boolean check {set;get;}
        
        public Accountwrapper(Account a){
            this.acc = a;
            check =false;
        }
    }
    
    public class Contactwrapper{
        Public Contact con {set;get;}
        public boolean check1 {set;get;}
        
        public Contactwrapper(Contact c){
            this.con = c;
            check1 =false;
        }
    }
}