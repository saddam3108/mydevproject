public class AccountContactInsert {
    public string aname{get;set;}
    public List<accountwrapper> accountwrplist{get;set;}
    public List<account> acclist{get;set;}
    public List<account> acclist1{get;set;}
    List<Contact> lstcon = new List<Contact>{};
        public Account account{get;set;}
    public Account a{get;set;}
    //public Account accountclone{get;set;}
    
    Public integer totalRecs = 0;
    Public integer OffsetSize = 0;
    Public integer LimitSize= 10;
    
    public AccountContactInsert(){
        account=new Account();
        acclist=new List<account>();
        //accountclone=new Account();
    }
    public void search() {
        OffsetSize = 0;
        if(accountwrplist == null){
            accountwrplist = new List<accountwrapper>();
        }else{
            accountwrplist.clear();
        }
        string soql='select id,Name,rating,phone,type,industry,(select id,name from contacts) from account where ';
        if(aname != null && aname != ''){
            soql=soql+' Name  LIKE  \'%' + string.escapeSingleQuotes(aname) + '%\'';
        }
        soql += ' limit : LimitSize OFFSET :OffsetSize';
        
        try{
            for(Account acc : Database.query(soql)){
                for(Contact child : acc.getSObjects('Contacts')) {
                    accountwrplist.add(new accountwrapper(acc,child));
                    system.debug('Child=' + child);
                }
                //accountwrplist.add(new accountwrapper(acc));
                system.debug('===== accountwrplist ====>'+accountwrplist);
            }
        } 
        catch(exception ex){
            string msg='Records cannot be searched with NULL values';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,msg));
        }
        
    }
    
    public PageReference selectedoffer(){
        
        acclist = new List<Account>();
        acclist1 = new List<Account>();
        if(accountwrplist.size()>0){
            for(accountwrapper w : accountwrplist){
                if(w.check == true){  
                    acclist.add(w.ac);
                    system.debug('---------- selected account list----->'+acclist);
                }
            }
        }
        PageReference nextpage;
        
        if(acclist.size()>0){
            nextpage = new PageReference('/apex/accountclone2');
            
        }
        else{
            nextpage = new PageReference('/apex/accountclone');
        }  
        return nextpage;
    }
    
    Public PageReference Insertaccount(){
        
        system.debug('=======account list=========>'+acclist);
        
        system.debug('--------offer list before update method------'+acclist);
        acclist1 = new List<Account>();
        //List<Contact> cons = new List<Contact>();
        //accountclone=new Account();
        try {
            
            for(account a:acclist)
            {
                
                Account accountclone = a.clone(false, false,true, true); 
                system.debug('===========a=============' + a);
                system.debug('===========accountclone=============' + accountclone);
                accountclone.name=account.name;
                accountclone.rating=account.rating;
                accountclone.phone=account.phone;
                accountclone.type=account.type;
                accountclone.industry=account.industry;
                
                acclist1.add(accountclone);  
                
                system.debug('===========account.name=============' + account.name);
                system.debug('===========accountclone.name=============' + accountclone.name);
                
                //***********************
                
                
                /*List<Contact> con = [SELECT Id, LastName, AccountId FROM Contact WHERE AccountId = : a.Id];
for(Contact c : con)
{
Contact conCopy = c.clone(false,true);
conCopy.AccountId = a.Id;
cons.add(conCopy);
}  */
                
            }
            
            insert acclist1;
            
            //insert cons;
            
            acclist1.clear();
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Offer Copied successfully<a href="/apex/accountclone2"><b>Click for More Update</b></a>'));
        } catch (DMLException e) {
            //system.debug('EXCEPTION '+e.getMessage() + ' ' + e.getLineNumber() +' '+e.getCause() );
            //ApexPages.addMessages(e);
        } 
        return null;
    }
    
    public pageReference cancel(){    
        PageReference pg=new PageReference('/001/o').setRedirect(true);
        
        Return pg ;
    }
    
    Public class accountwrapper{
        public account ac{get;set;}
        public boolean check{get;set;}
        public contact co {set;get;}
        
        public accountwrapper(account a, contact c){
            
            this.ac=a;
            check=false;
            this.co = c;
        }
    }
}