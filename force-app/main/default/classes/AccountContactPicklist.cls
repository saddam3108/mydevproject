public class AccountContactPicklist {
    public String SelectedAccountId {set;get;}
    
    public List<Selectoption> getAccountNames(){
        List<selectoption> option = new List<selectoption>();
        option.add(new selectoption('-None-','-None-'));
        List<Account> acc = [Select id,name from Account];
        for(Account a:acc){
            option.add(new selectoption(a.id,a.name));
        }
        system.debug('----Selectid--->'+SelectedAccountId);
        return option;
    }
    
    public List<Selectoption> getContactnames(){
        system.debug('----Selectid--->'+SelectedAccountId);
        List<Selectoption> conoption = new List<selectoption>();
        conoption.add(new selectoption('-None-','-None-'));
        if(SelectedAccountId !=null){
            for(Contact c : [select id, name from contact where accountId=:SelectedAccountId]){
                conoption.add(new selectoption(c.id,c.name));
            }
        }
        return conoption;
    }
}