public class AccountController {
    @AuraEnabled
    public static List<Account> findAll(){
        return[select id,name, Location__Latitude__s, Location__Longitude__s from Account where Location__Latitude__s != Null AND Location__Longitude__s != Null Limit 50];
    }
}