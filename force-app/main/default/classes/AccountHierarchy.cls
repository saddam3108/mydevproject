public class AccountHierarchy {
    public class Hierarchy{
        public Account grandAcc {set;get;}
        public List<Account> parentacc {set;get;}
        public Hierarchy(Account g, list<Account> acc){
            grandAcc = g;
            parentacc = acc;
        }
    }
    public List<Hierarchy> hier {set;get;}
    public List<Hierarchy> getmainnodes(){
        hier = new List<Hierarchy>();
        List<Account> acc = [select id,name from Account];
        for(Integer i=0;i<hier.size();i++){
            LisT<Account> acclist = [select id,name,(select id,name,phone from ChildAccounts) from Account];
            hier.add(new Hierarchy(acc[i],acclist));
        }
        return hier;
    }
}