public class AccountInsert {
	@AuraEnabled
    public static void doInsert(string aname, string aphone){
        Account a = new Account();
        a.Name = aname;
        a.Phone = aphone;
        insert a;
    }
}