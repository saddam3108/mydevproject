public class AccountMassUpdate {
    public List<Accountwrapper> accwrap {set;get;}
    public String aname {set;get;}
        public String aaname {set;get;}
    public Accountwrapper acccc {set;get;}
    public Account acc {set;get;}
    public List<Account> acclist {set;get;}
    public List<Account> aclist {set;get;}
    public AccountMassUpdate(){
    system.debug('------constructor -----');
        accwrap = new List<Accountwrapper>(); 
        acc = new Account();
        //acccc = new Accountwrapper();
        acclist = new List<Account>();
        aclist = new List<Account>();
    }
    
    public void doSearch(){
        for(Account al : [Select id,name,phone from account where name =: aname]){
            accwrap.add(new Accountwrapper(al));
        }
    }
    
    public Pagereference nextpage(){
        Pagereference p = new Pagereference('/apex/AccountMassUpdate_1_vf');
        return p;
    }
    
    public void doSave(){
        system.debug('----------Account List---------->'+accwrap);
        for(Accountwrapper acw : accwrap){
        system.debug('----------Account name---------->'+acw.ac.name);
            if(acw.checkbox == true){
            system.debug('----------select Account List---------->'+accwrap);
     
                acw.ac.name = aaname;
                system.debug('----------after name----------->'+acc.name);
                //acw.ac.phone = acc.Phone;
                aclist.add(acw.ac);
                system.debug('----------aclist list----------->'+aclist);
            }
        }
        update aclist;
    }
    
    public class Accountwrapper{
        public Boolean checkbox {set;get;}
        public account ac {set;get;}
        
        public Accountwrapper(Account a){
            ac = a;
            checkbox = false;
        }
    }
}