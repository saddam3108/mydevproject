public class AccountPickList {
    Public List<selectoption> option {set;get;}
    public AccountPickList(){
        option = new List<selectoption>();
    }
    Public List<selectoption> getpicklist(){
        for(Account a : [select name from account]){
            option.add(new selectoption(a.name,a.name));
        }
        return option;
    }
}