public class CallOutToInvoice {
    public InvoiceWrapper wrapper {set;get;}
    
    public void doCallOut(){
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://docsample.herokuapp.com/jsonSample');
        req.setHeader('Content-type', 'application/Json');
        req.setMethod('GET');
        
        HttpResponse res = h.send(req);
        system.debug('-----------response--------'+res.getBody());
        wrapper = (InvoiceWrapper)Json.deserializeStrict(res.getBody(), InvoiceWrapper.class);
    }
}