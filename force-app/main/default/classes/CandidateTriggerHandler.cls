public class CandidateTriggerHandler {
    public Static void doSave(List<Candidate__c> clist){
        List<solution>  slist = new List<Solution>();
        for(Candidate__c c:clist){
            solution s = new Solution();
            s.SolutionName = c.First_Name__c;
            s.SolutionNote = c.Description__c;           
            slist.add(s);
        } insert slist;
    }
    
    public Static void doUpdate(List<Candidate__c> clist){
        Set<Id> ids = new Set<Id>(); 
        for(Candidate__c cval : clist){
            ids.add(cval.SolutionName__c);
        }
        List<Solution> slist = new List<Solution>();
        for(Solution sl : [select id, solutionname,SolutionNote from solution where id In : ids])
        {
            for(Candidate__c cval : clist){
                sl.SolutionNote = cval.Description__c;
                slist.add(sl);
            }
        } update slist;
    }
}