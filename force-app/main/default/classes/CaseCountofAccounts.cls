public class CaseCountofAccounts {

    public Map<String, Integer> casecountmap {set;get;}
    Public List<aggregateresult> result {set;get;}
    
    public CaseCountofAccounts(){
    casecountmap = new Map<String, Integer>();
    result = [SELECT count(Id) cnt, Account.Name an FROM Case Group By Account.Name];
    for(aggregateresult r : result){
        casecountmap.put((string)r.get('an'),(integer)r.get('cnt'));
       }
    }
}