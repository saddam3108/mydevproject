global class CaseCountusing_batchapx implements Database.Batchable<SObject> 
{
    public Set<Id> accids ;
    public List<Account> acclist ;
    
    public CaseCountusing_batchapx()
    {
        accids=new Set<Id>();
        acclist=[select id from Account];
        for(Account i:acclist)
        {
            accids.add(i.id);
        }
    }
    
    global Database.QueryLocator start(Database.BatchableContext context) 
    {
        return Database.getQueryLocator([select Id,	OpenCases__c,ClosedCases__c,(select id,status from cases) from Account where id IN :accids]);
    }
    
    global void execute(Database.BatchableContext context, List<Account> scope) 
    {
        List<Account> acc=new List<Account>();
        for(Account a:scope)
        {  
            integer i=0;
       		integer j=0;
            integer count=0;
            for(case c:a.cases)
            {
                count=count+1;
                system.debug('---------loop count-------'+count);
                system.debug('-------case id------------>'+c.id);
                system.debug('-------Account id------------>'+a.id);
                if(c.status =='Closed')
                {
                    i=i+1;
                    system.debug('------i value-------->'+i);
                    a.ClosedCases__c=i;
                }
                if(c.status !='Closed')
                {
                    j=j+1;
                    system.debug('------j value-------->'+j);
                    a.OpenCases__c=j;
                }
            } 
            acc.add(a);
        }
        update acc;
    }
    
    global void finish(Database.BatchableContext context) {
    }
}