@isTest
public class CaseCountusing_batchapx_Test 
{
    public static testmethod void batchtest()
    {
        Account a=new Account();
        a.Name='Test';
        insert a;
        Case c=new Case();
        c.status='closed';
        c.AccountId=a.id;
        insert c;
        Case c1=new Case();
        c1.Status='New';
        c1.AccountId=a.id;
        insert c1;
        
        Test.startTest();
        CaseCountusing_batchapx b=new CaseCountusing_batchapx();
        database.executeBatch(b);
        Test.stopTest();
    }
}