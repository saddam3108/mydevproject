public class ChartController {
    public List<datawrapper> wrlist {set;get;}
    public integer num1 {set;get;}
    public integer num2 {set;get;}
    public ChartController(){
        num1 =0;
        num2 =0;
    }
    
    public List<datawrapper> getData(){
        wrlist = new List<datawrapper>();
        wrlist.add(new datawrapper('2012',num1));
        wrlist.add(new datawrapper('2013',num2));
        return wrlist;
    }
    
    public class datawrapper{ 
        public string year{set;get;}
        public integer num {set;get;}
        
        public datawrapper(string year, integer n){
            this.year = year;
            this.num=n;
        }
    }
    
}