global class CloneAccount {
    webservice static void cloneAccts(Id accid){
        Account acc = [Select id,name from Account where Id=: accid];
        account ac = acc.clone(false,true);
        ac.Name = acc.Name+'-'+system.today();
        insert ac;
        List<Contact> con = new List<Contact>();
        for(Contact co : [Select id,lastname, AccountId from Contact where AccountId=:accid]){
            Contact copycon = co.clone(false, true);
            copycon.AccountId=ac.Id;
            con.add(copycon);
        }
        insert con;
    }
}