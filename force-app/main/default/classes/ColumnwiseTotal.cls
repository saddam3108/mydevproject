public class ColumnwiseTotal {
    public List<Target__c> tlist {set;get;}
    
    public ColumnwiseTotal()
    {
        tlist=[select id,name,TargetAmount__c,TargetAmount1__c from Target__c where TargetAmount__c != null AND TargetAmount1__c != null];
    }
}