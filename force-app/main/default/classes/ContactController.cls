public class ContactController {
    
    @AuraEnabled
    Public Static List<Contact> get10contact(){
        return [Select id,name from contact limit 10];
    }
}