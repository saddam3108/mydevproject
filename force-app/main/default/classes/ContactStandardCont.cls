public class ContactStandardCont {
    Public List<Account> acc {set;get;}
     Public List<WrapperClassEx> WrapperList{get;set;}
    public ContactStandardCont(ApexPages.StandardController controller) {
      
    }
   
   Public List<WrapperClassEx> getwrapperObj(){
      List<Account> accList = [Select id,name from account limit 5];
      WrapperList = New List<WrapperClassEx>();
      for(Account acc: accList){
        WrapperList.add(New WrapperClassEx(acc,false)); 
      }
      return WrapperList;
   }
    Public PageReference selectedAccounts(){
        acc = new List<Account>();
        for(WrapperClassEx w : WrapperList){
            if(w.checkBox == true){
                acc.add(w.accObj);
            }
        }
        PageReference p =new PageReference('/apex/ContactStandardCont_Page_1');
        return p;
    }
    
  
   Public Class WrapperClassEx{
     Public Account accObj{get;set;}
     Public Boolean checkBox{get;set;}
    
     Public WrapperClassEx(Account accRec, boolean SelectBox){
        accObj = accRec;
        checkBox = SelectBox;
     }
   }
}