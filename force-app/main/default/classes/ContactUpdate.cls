public class ContactUpdate {
    public static void doUpdate(List<Account> acc){
        set<Id> ids =new set<Id>();
        for(Account ac: acc){
            ids.add(ac.id);
        }
        system.debug('-----------ids------->'+ids);
        List<Contact> con =[select id, phone,accountId from contact where accountid IN:ids];
        system.debug('--------------Con--------->'+Con);
        for(contact c : con){
            for(Account a : acc){
                c.phone = a.phone;
            }
        }
        update con;
    }
    
    public static void doupdate1(Map<id,Account> accmap){
        set<Id> ids = accmap.keyset();
        List<Contact> con = [select id,phone,accountid from contact where AccountId in : ids];
        for(Contact c:con){
            c.phone = accmap.get(c.accountid).phone;
        }
        update con;
    }
}