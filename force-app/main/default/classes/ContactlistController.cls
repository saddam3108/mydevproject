public class ContactlistController {
@AuraEnabled
    public static List<Contact> getcon(string cname){
        List<Contact> returncon = new List<Contact>();
        List<Contact> conlist = [Select id,name,phone from contact where name =: cname];
        for(Contact c : conlist){
            returncon.add(c);
        }
        return returncon;
    }
}