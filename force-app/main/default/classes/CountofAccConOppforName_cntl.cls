public class CountofAccConOppforName_cntl {

    public string aname {set;get;}
    public string aid {set;get;}
    public List<Account> acclist {set;get;}
    public List<Contact> conlist {set;get;}
    public List<Opportunity> opplist {set;get;}
    public Boolean flag {set;get;}
    
    public void search(){
    flag = true;
     acclist = new List<Account>();
     conlist = new List<Contact>();
     opplist = new List<Opportunity>();
     string stext= '*'+aname+'*';
     string query = 'Find\''+stext+'\' In All Fields Returning Account(name,email__c),Contact(Lastname,Email),Opportunity(Name,email__c)';
     List<List<sobject>> slist = search.query(query);
       accList = ((List<Account>)slist[0]);
       conList  = ((List<contact>)slist[1]);
       opplist = ((List<Opportunity>)slist[2]); 
    }
    
    public void sendMail(){
       List<String> strs = new List<String>();
       strs.add(aid);

         Messaging.SingleEmailMessage varmail = new Messaging.SingleEmailMessage();
         varmail.setToAddresses(strs);
         varmail.setSubject('Record Email id');
         varmail.setPlainTexTBody('Hi..... This is test mail');
         Messaging.SendEmail(New Messaging.SingleEmailMessage[]{varmail});
    } 
}