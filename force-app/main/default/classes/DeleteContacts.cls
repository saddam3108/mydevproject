public class DeleteContacts {
    @AuraEnabled
    public static List<Contact> fectconts(){
        List<Contact> returncont = new List<Contact>();
        for(Contact c : [select id,name,phone from contact]){
            returncont.add(c);
        }
        return returncont;
    }
    @AuraEnabled
    public static List<String> delcont(List<String> lstRecordId){
        List<String> errmsg = new List<String>();
        List<Contact> delcon = [Select id from Contact where Id IN : lstRecordId];
        Database.DeleteResult[] delres = Database.delete(delcon,false);
        for(Database.DeleteResult dr : delres){
            if(dr.isSuccess()){
                system.debug('Successfully deleted');
            } else {
                errmsg.add('');
                for(Database.Error er : dr.getErrors()){
                    errmsg.add(er.getStatusCode() +':'+ er.getMessage());
                }
            }
        }
        return errmsg;
    }
}