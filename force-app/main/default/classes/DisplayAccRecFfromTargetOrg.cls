public class DisplayAccRecFfromTargetOrg {
    public string accountname {set;get;}
    public Boolean check {set;get;}
    Public DisplayAccRecFfromTargetOrg(){
		check = false;
    }
    
    Public void doSearch(){
        // establish the connection to the Target (saddam@trailhead.com)
        check = true;
        partnerSoapSforceCom.Soap connection = new partnerSoapSforceCom.Soap();
        partnerSoapSforceCom.LoginResult loginresult = connection.login('saddam@trailhead.com','123456@s');
        
        system.debug('------Login result------'+loginresult);
        system.debug('------session id-------'+loginresult.sessionId);
        
        String sessionid = loginresult.sessionId;
        soapSforceComSchemasClassAccountrec.SessionHeader_element webserviceheader = new soapSforceComSchemasClassAccountrec.SessionHeader_element();
    	webserviceheader.sessionId = sessionid;
        
        //soapSforceComSchemasClassAccountrec.fetchRecordsResponse_element respEle = new soapSforceComSchemasClassAccountrec.fetchRecordsResponse_element();
        
        soapSforceComSchemasClassAccountrec.Accountrecs getrecords = new soapSforceComSchemasClassAccountrec.Accountrecs();
        getrecords.SessionHeader = webserviceheader;
        
        List<soapSforceComSchemasClassAccountrec.wrap> result= getrecords.findrec(accountname);
        system.debug('----'+result); 
        
        wrapinner = new List<wrapInner>();
        for(soapSforceComSchemasClassAccountrec.wrap ww : result){
            wra = new wrapInner();
            wra.name = ww.name;
            wra.phone = ww.phone;
            wrapinner.add(wra);
        }
    }
    
    public List<wrapInner> wrapinner {set;get;}
    public wrapInner wra {set;get;}
    
    public class wrapInner {
        Public string name {set;get;}
        public string phone {set;get;}
    }
}