public class DisplayAccountListView {
    public List<wrapAccount> wrapAccountList {get; set;}
    public List<Account> selectedAccounts{get;set;}
    Public List<wrapAccount> searchlist {set;get;}
    Public String aname {set;get;}
    
    private integer totalRecs = 0;     
    private integer index = 0;
    private integer blockSize = 5;  
    
    public DisplayAccountListView(){
    
          totalRecs = [select count() from Account]; 
            
              searchlist = new List<wrapAccount>();
            wrapAccountList = new List<wrapAccount>();
            List<Account> Accs = Database.Query('SELECT Name,BillingState,Phone FROM Account LIMIT :blockSize OFFSET :index');
            totalRecs = Accs.size();
            
            system.debug('-----------total recs-------'+totalRecs);
            
            //List<Account> Accs = Database.Query('SELECT Name,BillingState,Phone FROM Account');
  
           // for(Account a: [select Id, Name,BillingState, Website, Phone from Account Limit 10]) {
              //  wrapAccountList.add(new wrapAccount(a));
          //  }
            system.debug('---------------controller list of accounts----------'+Accs.size());
            
            for(Account a: Accs) {
                wrapAccountList.add(new wrapAccount(a));
                
            }
            
        }
        
        /*public List<wrapAccount> getAccs() 
        {
            searchlist = new List<wrapAccount>();
            wrapAccountList = new List<wrapAccount>();
            List<Account> Accs = Database.Query('SELECT Name,BillingState,Phone FROM Account LIMIT :blockSize OFFSET :index');
            
            for(Account a: Accs) {
                wrapAccountList.add(new wrapAccount(a));
            }
            
            return wrapAccountList;
        }     */  
    
    public PageReference processSelected() {
        selectedAccounts = new List<Account>();
        for (wrapAccount w : wrapAccountList) {
            if (w.selected == true) {
                selectedAccounts.add(w.acc);
            }
        }
        PageReference pdfPage = new PageReference('/apex/DisplayAccountListView_1_Page');
        return pdfPage;
    }
    
    Public PageReference doUpdate(){
    update selectedAccounts;
    /*PageReference OpenNewPage = Page.DisplayAccountListView_Page;
    OpenNewPage.setRedirect(true);
    return OpenNewPage;*/
    PageReference p = new PageReference('https://hussain123-dev-ed.my.salesforce.com/001?fcf=00B6F00000AzucR');
        return p;
    }
    
    Public void doSearch(){
        for(wrapAccount ac : wrapAccountList){
        system.debug('===Name========>'+ac);
        if(ac.acc.Name.containsIgnoreCase(aname)){
            
            searchlist.add(ac);
            system.debug('---------Search List------>'+searchlist);
            }
        } 
        wrapAccountList.clear();
        wrapAccountList = searchlist.clone();
        //totalRecs=searchlist.size();
    }
    
    Public class wrapAccount{
        public Account acc {get; set;}
        public Boolean selected {get; set;}
        
        public wrapAccount(Account a) {
            acc = a;
            selected = false;
        }
    }
    
    public void beginning(){
        index = 0;
    }
    
    public void previous(){
         index = index - blockSize;
    }
    
    public void next(){
        system.debug('------index first value-------------'+index);
        system.debug('------blocksize first value-------------'+blockSize);
        index = index + blockSize;
        system.debug('------index after value-------------'+index);
    }
    
    public void end(){
        index = totalrecs - math.mod(totalRecs,blockSize);
    }
    
    public boolean getprev(){
        if(index == 0)
        return true;
        else
        return false;
    }
    
    public boolean getnxt(){
        if((index + blockSize) > totalRecs)
        return true;
        else
        return false;
    }
}