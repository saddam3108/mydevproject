public class DisplayAllMonthsBetweenDates {
     public list<Date> monthYearList {get;set;}
    public date startDate {get;set;}
    public date endDate {get;set;}
    public DisplayAllMonthsBetweenDates () {
        Set<Date> monthYearSet = new Set<Date>();
        monthYearList = new list<Date>();
        startDate = date.newInstance(2015, 1, 17);
        endDate = date.newInstance(2015, 12, 17);
        while(startDate <= endDate) {
            monthYearSet.add(startDate);
            startDate = startDate.AddMonths(1);
        }
        monthYearList.addAll(monthYearSet);
       // monthYearList.sort();
    }
}