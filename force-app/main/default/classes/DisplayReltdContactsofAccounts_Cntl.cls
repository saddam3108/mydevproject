public class DisplayReltdContactsofAccounts_Cntl {
    public Map<String, List<Contact>> displaymap {set;get;}
    
    public DisplayReltdContactsofAccounts_Cntl(){
        displaymap = new Map<String, List<Contact>>();
        
        for(Contact c:[select account.name,lastname,leadsource from contact]){
        
            if(!displaymap.containskey(c.account.name)){
            displaymap.put(c.account.name, new List<Contact>());
            }
            if(displaymap.containskey(c.account.name)){
                if(c.account.name !=null)
            displaymap.get(c.account.name).add(c);
            }
        }
    }
}