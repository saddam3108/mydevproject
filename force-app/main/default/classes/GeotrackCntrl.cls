public with sharing class GeotrackCntrl {


    public GeotrackCntrl(ApexPages.StandardController controller) {
    
    }
    @RemoteAction
    
    public static List<Contact> getNearbyContacts(Decimal clat, Decimal clong) {
        String q = 'SELECT Name FROM Contact WHERE DISTANCE(Location__c, GEOLOCATION('+String.valueOf(clat)+','+String.valueOf(clong)+'), "mi") < 10';
        List<Contact> contacts = Database.query(q);
        return contacts;

    }

}