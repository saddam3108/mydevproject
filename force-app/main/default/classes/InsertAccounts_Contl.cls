public class InsertAccounts_Contl {
    public string name {set;get;}
    public string phone {set;get;}
    public List<Account> acc {set;get;}
    
    public InsertAccounts_Contl(){
        acc = new List<Account>();
        acc.add(new Account());
        }
    
    public void doAdd(){
        acc.add(new Account());
      }
      
    public void doInsert(){
        Database.SaveResult[] result=Database.insert(acc,false);

        for(Database.SaveResult rs :result){
                if(rs.isSuccess()){
                    System.debug('Record id:'+rs.getId());
                }else{
                    Database.Error[] errors=rs.getErrors();
                    for(Database.Error er :errors){
                            System.debug('Fields :'+er.getFields());
                            System.debug('Error Message :'+er.getMessage());
                    }
                }
            }
    }
}