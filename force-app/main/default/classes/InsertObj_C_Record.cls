public class InsertObj_C_Record 
{
    public Map<String, List<Object_A__c>> amap {set;get;}
    public List<Object_A__c> objA1 {set;get;}
    public Object_B__c objB {set;get;}
    public Object_C__c objC {set;get;}
    public Date StartDate {set;get;}
    public Date EndDate {set;get;}
    public Integer selectedyear {set;get;}
    public Boolean flag {set;get;}
    
        public List<SelectOption> getYears(){
        List<Selectoption> options=new List<Selectoption>();
        for (Integer i = System.Today().year() - 3; i <= System.Today().year() ; i++) {
            options.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
        }
        return options;
    }

    public InsertObj_C_Record(){
        amap = new Map<String, List<Object_A__c>>();
        objB = new Object_B__c();
        objC = new Object_C__c();
    }
    
    public void getObjB(){       
        objB = [ Select id,name,Active__c,UnitPrice__c from Object_B__c where id =: objC.Object_B_Name__c ];  
        if(objB.Active__c == true)
        objC.UnitPrice__c =objB.UnitPrice__c;
        system.debug('---------Object B--->'+objB.UnitPrice__c ); 
    }
    
    public void doSave(){
        insert objC;   
    }
    
    public void getrecods(){
     flag=true;
       amap.clear();
        String s = 'select name,Date__c,No_Of_Items__c,TotalAmount__c from Object_A__c where ';
        if(selectedYear!=null)
        {
       
            string s1=s+'CALENDAR_year(Date__c)=:selectedYear';
            for(Object_A__c o :Database.query(s1)){
                
                DateTime d = o.Date__c;
                String monthName = d.format('MMMMM');
                
                if(!amap.containskey(monthName)){
                    amap.put(monthName, new List<Object_A__c>{o});
                }
                else{
                    amap.get(monthName).add(o);
                }
            }  
        } 
          
                  
        if(selectedYear!=null && (startdate!=null || enddate!=null))
        {
            if(Startdate!=null )
                s=s+'  Date__c>=:Startdate '; 
            if(Enddate!=null)
            {
                if(Startdate!=null)       
                s=s+' AND Date__c<=:Enddate ';  
                
            }
            objA1 = Database.query(s);
        }
            
    }
}