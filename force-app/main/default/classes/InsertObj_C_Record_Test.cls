@isTest
public class InsertObj_C_Record_Test {
    
    public static testmethod void recordtest()
    {
        List<Object_A__c> alist=new List<Object_A__c>();
        Object_A__c a=new Object_A__c();
        a.Date__c=system.today();
        insert a;
        
        System.Debug('----a data-------'+a);
        
        
        Object_B__c b=new Object_B__c();
        b.UnitPrice__c=100;
        b.Active__c=true;
        insert b;
        
        System.Debug('----b data-------'+b);
        
        
     /*   Object_C__c c=new Object_C__c();
        c.Object_A_Name__c = a.Id;  
        c.Object_B_Name__c = b.Id;
        c.UnitPrice__c = 100;
        c.Quantity__c = 1;
        insert c;
        
        System.Debug('----c data-------'+c);*/
        
        InsertObj_C_Record  cls = new InsertObj_C_Record ();
        cls.StartDate=system.today();
        cls.EndDate=system.today();
        cls.selectedyear=2017;
        cls.flag=true;
        cls.getYears();
        cls.objC.Object_B_Name__c=b.id;
        cls.getObjB();
         cls.objC.Object_A_Name__c =a.id;
        cls.doSave();
        cls.getrecods();   
    }
}