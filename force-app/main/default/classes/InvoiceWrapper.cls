public class InvoiceWrapper {
    public List<Invoice> invoicelist {set;get;}
    public class Invoice{
        public double totalprice {set;get;}
        public DateTime statementDate{set;get;} 
        public List<LineItem> lineItems{set;get;}
        public Integer invoiceNumber {set;get;}
    }
    
    public class LineItem{
        public double unitprice {set;get;}
        public double quantity {set;get;}
        public string ProductName {set;get;}
        
        public Double getLineItemTotal() {
            return this.unitPrice * this.quantity;
        }
    }
}