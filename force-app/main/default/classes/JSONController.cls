public class JSONController {
    public string parser {set;get;}
    Public list<Account> acc {set;get;}
    
    public JSONController(){
        String soql = 'SELECT Name,phone, industry FROM Account LIMIT 5';
        List<Account> acct = Database.Query(soql); 
        parser = JSON.serialize(acct);
        acc = (List<Account>) system.JSON.deserialize(parser, list<Account>.class);
    }
}