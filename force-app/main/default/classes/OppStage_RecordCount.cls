public class OppStage_RecordCount {
    Public Map<String, Integer> stagecount {set;get;}
    //Public List<Aggregateresult> result {set;get;}
    
    public OppStage_RecordCount(){
    
    stagecount = new Map<String, Integer>();
    
    for(Aggregateresult r : [select count(Id) cnt, stagename sn from Opportunity Group By stagename]){
        stagecount.put((string)r.get('sn'), (integer)r.get('cnt'));
        }
    }
}