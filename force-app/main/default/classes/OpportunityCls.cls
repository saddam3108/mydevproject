public class OpportunityCls {
    @future(callout = true)
    Public static void insertmthd(id d){
        Opportunity op = [Select name, description, account.name from opportunity where id =: d];
        
        Opp_partnerSoapSforceCom_2.soap obj1 = new Opp_partnerSoapSforceCom_2.soap();
        Opp_partnerSoapSforceCom_2.LoginResult lr = obj1.login('saddam@trailhead','123456@s');
        
        Opp_soapSforceComSchemasClass_1.WebserviceOpportunityCls obj = new Opp_soapSforceComSchemasClass_1.WebserviceOpportunityCls();
        obj.sessionHeader = new Opp_soapSforceComSchemasClass_1.SessionHeader_element();
        obj.sessionHeader.sessionid = lr.sessionId;
        obj.doInsert(op.Name, op.Description, '0016F000025tNeB');
    }
}