public class Opportunity_Report {
    public List<String> LeadSourcelist { get; set; }
    public List<String> StageNamelist { get; set; }
    public Map<String,Integer> LeadCountMap { get; set; }
    public Map<String,Integer> StageCountMap { get; set; }
    public Map<String,Map<String,Integer>> OppCountMap { get; set; }
    
    public Opportunity_Report () {
        OppCountMap = new Map<String,Map<String,Integer>>();
        StageCountMap = new Map<String,Integer>();
        LeadCountMap = new Map<String,Integer>();
        LeadSourcelist = new List<String>();
        StageNamelist = new List<String>();
        
        Schema.DescribeFieldResult fieldResult1 = Opportunity.LeadSource.getDescribe();
        List<Schema.PicklistEntry> ple1 = fieldResult1.getPicklistValues();
        for( Schema.PicklistEntry f: ple1){
            LeadSourcelist.add(f.getValue());
            //LeadCountMap.put(f.getValue(),0);
            //LeadCountMap.put('Total',0);
        }
        
        Schema.DescribeFieldResult fieldResult2 = Opportunity.StageName.getDescribe();
        List<Schema.PicklistEntry> ple2 = fieldResult2.getPicklistValues();
        for( Schema.PicklistEntry f : ple2){
            StageNamelist.add(f.getValue());
            StageCountMap.put(f.getValue(),0);
            //StageCountMap.put('yTotal',0); // Footer
        }
        
        //Integer i = 0;
        for(Opportunity opp : [ select id,name,LeadSource,StageName from Opportunity ]){
            
            if(! OppCountMap.containskey(opp.LeadSource) ){
                OppCountMap.put(opp.LeadSource, New Map<String,Integer>() );
                //StageCountMap.put(opp.LeadSource,0);
                }
            for(String SN : StageNamelist){    
                if(! OppCountMap.get(opp.LeadSource).containskey(SN) )
                    OppCountMap.get(opp.LeadSource).put(SN, 0);
                  //  OppCountMap.get(opp.LeadSource).put('yTotal', 0);
            }
            if( OppCountMap.get(opp.LeadSource).containskey(opp.StageName) ){
                OppCountMap.get(opp.LeadSource).put(opp.StageName, OppCountMap.get(opp.LeadSource).get(opp.StageName)+1);
            }
            StageCountMap.put(opp.StageName,StageCountMap.get(opp.StageName)+1);
            //StageCountMap.put('yTotal',StageCountMap.get(opp.StageName));
           
        }
        
        for(String ls : OppCountMap.keyset()){
        Integer i = 0;
            for(String sn : OppCountMap.get(ls).keyset()){
                i = i + OppCountMap.get(ls).get(SN);
            }
        OppCountMap.get(ls).put('yTotal', i);
        
        }
        
        integer i=0;
        for(string sn : StageCountMap.keyset()){
        
        i=i + StageCountMap.get(sn);
        system.debug('----------------i-----------'+i);
        StageCountMap.put('yTotal',i);
        }
    }
}