public class PaginationWithFilter {  
    public string aname {set;get;}
    Public integer totalRecs = 0;     
    Public integer index = 0;
    Public integer blockSize = 5; 
    Public List<Account> acc {set;get;}
    
    public PaginationWithFilter()
    {
        totalRecs = [select count() from Account]; 
    }    
    
    public void doget() 
    {
        string soql = 'SELECT Name, phone FROM Account where ';
        if(aname != null){
            soql = soql + ' name LIKE \'%' + string.escapeSingleQuotes(aname) + '%\'';
        }
        soql += ' LIMIT :blockSize OFFSET :index';
        acc = Database.query(soql);
       
        System.debug('Values are ' + acc);
     
    }    
    
    public void beginning()
    {
        index = 0;
        system.debug('-----------Index----------->'+index);
    }
    
    public void previous()
    {
        system.debug('-----------previous Index----------->'+index);
        system.debug('-----------blockSize----------->'+blockSize);
        index = index - blockSize;
        doget();
        system.debug('-----------previous after Index----------->'+index);
    }
    
    public void next()
    {system.debug('----------- next Index----------->'+index);
        system.debug('-----------blockSize----------->'+blockSize);
        index = index + blockSize;
     doget();
     system.debug('-----------next after Index----------->'+index);
    }

    public void end()
    {
        index = totalrecs - math.mod(totalRecs,blockSize);
    }        
    
    public boolean getprev()
    {
        if(index == 0)
        return true;
        else
        return false;
    }  
    
    public boolean getnxt()
    {
        if((index + blockSize) > totalRecs)
        return true;
        else
        return false;
    }          
}