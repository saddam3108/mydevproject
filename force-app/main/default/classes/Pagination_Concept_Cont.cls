public class Pagination_Concept_Cont {
    //Public String aname {set;get;}
    Public String fname {set;get;}
    Public String aphone {set;get;}
    public List<CCWRow> tRecords{get;set;}
    public List<Contact> conlist{get;set;}
    private integer totalRecs = 0;
    Public integer OffsetSize = 0;
    Public integer LimitSize= 3;
    
    public Pagination_Concept_Cont ()
    {
        totalRecs = [select count() from contact];
    }
    
    Public void doSearch(){
        try{
            
            if(tRecords == null){
                tRecords = new List<CCWRow>();
            }else{
                tRecords.clear();
            }
            String soql = 'SELECT Id, FirstName, LastName, Account.name, Title, Phone, MobilePhone, Email FROM Contact where';
            
            if(aphone!=null && aphone!='')
            {
                soql=soql+' Phone =:aphone';
            }   
            
            if(fname!= null && fname!='')  
            {
                if(aphone!=null && aphone!='')
                {
                    soql=soql+' and' ;
                }
                soql=soql+' FirstName LIKE \'%' + String.escapeSingleQuotes(fname) + '%\'';
            }
            soql += ' limit : LimitSize OFFSET :OffsetSize';
            system.debug('--------final query-----------'+soql);
            for(Contact con : Database.query(soql)){
                tRecords.add(new CCWRow(con));
            }
            //totalRecs++;
            
        }
        catch(exception ex){
            string msg='Records cannot be searched with NULL values';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING,msg));
        }   
    }
    
    public PageReference selectedcons(){
        conlist = new List<Contact>();
        if(trecords.size()>0){
            for(CCWRow cval : trecords){
                if(cval.IsSelected == true){
                    conlist.add(cval.tcontact);
                }
            }
        }  
        PageReference nextpage;
        
        if(conlist.size()>0){
            nextpage = new PageReference('/apex/Pagination_concept_1');
        }
        else{
            nextpage = new PageReference('/apex/Pagination_concept');
        }  
        return nextpage;
    }
    
    Public PageReference doUpdate(){
        try {
            update conlist;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Updated successfully <a href="/apex/Pagination_concept"><b>More Update</b></a>'));
        } catch (DMLException e) {
            ApexPages.addMessages(e);
        }
        return null;
    }
    
    public void beginning()
    {
        OffsetSize = 0;
        system.debug('-----------Index----------->'+OffsetSize);
    }
    
    
    public void next(){
        OffsetSize = OffsetSize + LimitSize;
        dosearch();
        
    }
    
    public void previous(){
        OffsetSize = OffsetSize-LimitSize;
        dosearch();
    }
    
    public boolean getprev(){
        if(OffsetSize == 0){
            return true;
        }
        else {
            return false;
        }
    }
    
    public boolean getnxt(){
        
        system.debug('----OffsetSize---0-'+OffsetSize);
        system.debug('----LimitSize--5--'+LimitSize);
        system.debug('----totalRecs--0--'+totalRecs);
        if((OffsetSize + LimitSize) > totalRecs)
            return true;
        
        else 
            return false;
        
    }
    
    public with sharing class CCWRow{
        
        public Contact tContact{get;set;}
        public Boolean IsSelected{get;set;}
        
        public CCWRow(Contact c){
            this.tContact=c;
            this.IsSelected=false;
        }        
    } 
}