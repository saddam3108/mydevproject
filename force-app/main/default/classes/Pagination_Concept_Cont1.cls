public class Pagination_Concept_Cont1 {
    Public List<Account> acc {set;get;}
    private integer totalRecs = 0;     
    private integer index = 0;
    private integer blockSize = 5;         
    
    public Pagination_Concept_Cont1()
    {
             
    }    
    
    public void getMemb() 
    {
        acc = [SELECT Name, phone FROM Account LIMIT :blockSize OFFSET :index];
        System.debug('Values are ' + acc);
        
    }    
    
    public void beginning()
    {
        index = 0;
    }
    
    public void previous()
    {
        index = index - blockSize;
        getMemb();
    }
    
    public void next()
    {
        index = index + blockSize;
        getMemb();
    }
    
    public void end()
    {
        index = totalrecs - math.mod(totalRecs,blockSize);
    }        
    
    public boolean getprev()
    {
        if(index == 0)
            return true;
        else
            return false;
    }  
    
    public boolean getnxt()
    {
        if((index + blockSize) > totalRecs)
            return true;
        else
            return false;
    }         
}