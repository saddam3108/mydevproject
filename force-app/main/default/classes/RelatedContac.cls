public class RelatedContac
{    
    public String accName { get; set; }
    public List<Account> lstaccount {set;get;}
    public List<contact> lstcontacts {set;get;}
    public set<string> accIds     {set;get;}
    
    public RelatedContac()
    {
      lstaccount = new List<Account>();
      lstcontacts = new List<contact>();
         accIds  = new set<string>();
    }
    
    public List<contact> getConRecords()
    {
       lstcontacts.clear();
       accIds.clear();
       lstaccount.clear();
       system.debug('****AccNameTextValue is *****'+accName);
       lstaccount=[select id,name from Account where name=:accName];
       for(Integer i=0;i<lstaccount.size();i++)
       {
           accIds.add(lstaccount[i].Id);
       }
       
        lstcontacts =[select id,name,accountId from contact where accountid in : accIds];
        system.debug('**** List of Contacts for Test is ***'+lstcontacts);
        return lstcontacts;
    }

    public pagereference showContacts() 
    {
       return null;
        
    }
}