@RestResource(urlMapping='/Account/*')
global class RestApiEx {
    @HttpGet
    global static List<Account> doGet() {
        RestRequest req = RestContext.request;
        //RestResponse res = RestContext.response;
        String SLAnumber = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        system.debug('-------phone number---'+SLAnumber);
        List<Account> result = [SELECT Id, Name, Phone, Website,Industry,Type, Annualrevenue, SLASerialNumber__c from Account where SLASerialNumber__c =: SLAnumber];
        return result;
    }
    
    @HttpPut
    global static string doPut() {
        RestRequest req = RestContext.request;
        //integer DepositAmount = Integer.valueOf(RestContext.request.params.get('amount'));
        String SLAnumber = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        system.debug('-------phone number---'+SLAnumber);
        Account result = [SELECT Id, Name, Phone, Website,Industry,Type, Annualrevenue, SLASerialNumber__c from Account where SLASerialNumber__c =: SLAnumber];
        result.Phone= SLAnumber;
        update result;
        return 'Account Update';
    }
    
}