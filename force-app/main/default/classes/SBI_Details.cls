public class SBI_Details {
    public SBIAccount__C sbiac {set;get;}
    public Boolean flag {set;get;}
    
    public SBI_Details(){
        sbiac = new SBIAccount__C();
        flag = false;
    }
    
    public void popup(){
        system.debug('-------flag before----->'+flag);
        flag = true;
        system.debug('-------flag after----->'+flag);
    }
    
    public void createAcc(){
        try{
            insert sbiac;
        } catch(Exception e)
        {}
    }
    
  /*  public string acname {set;get;}
    public string fname {set;get;}
    public string mname {set;get;}
    public date dob {set;get;}
    public string acphone {set;get;}
    public string acemail {set;get;}*/
}