public class Salesvist_Cntrlr {
    public List<Opportunity> opplist {set;get;}
    public List<Sales_Visit__c> saleslist {set;get;}
    Public Set<ID> ids {set;get;}
    public Salesvist_Cntrlr(){
        ids=new Set<Id>();
        opplist=[select id from opportunity];
        for(opportunity opp :opplist){
            ids.add(opp.id);
        }
        saleslist = [select name,Latitude__c,Longitude__c from Sales_Visit__c where OppName__c In:ids];
        system.debug('---------->'+saleslist);
    }
}