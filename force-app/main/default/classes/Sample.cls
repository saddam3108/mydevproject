public class Sample
{
    
    public Integer selectedyear {set;get;}
    public List<Object_A__c> Alist {set;get;}
    public Map<String, List<Object_A__c>> acmap {set;get;}
    
    public List<SelectOption> getYears(){
        List<Selectoption> options=new List<Selectoption>();
        for (Integer i = System.Today().year() - 3; i <= System.Today().year() ; i++) {
            options.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
        }
        return options;
    }
    public Sample(){
        Alist = new List<Object_A__c>(); 
        acmap = new Map<String, List<Object_A__c>>();
        
    }
    public void submit(){
        if(selectedYear <> null){
            for(Object_A__c o : [ Select name,Date__c from Object_A__c where CALENDAR_year(Date__c)=: selectedYear order By Date__c ASC ]){
                
                DateTime d = o.Date__c;
                String monthName = d.format('MMMMM');
                system.debug('------month name--->'+monthname);
                if(monthName > 'January'){
                if(!acmap.containskey(monthName)){
                    acmap.put(monthName, new List<Object_A__c>{o});
                }
                else{
                    acmap.get(monthName).add(o);
                }
            }
            }
        }   
    }
}