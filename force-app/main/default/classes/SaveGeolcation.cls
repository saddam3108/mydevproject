public class SaveGeolcation {
 @AuraEnabled
    public static task  doInsert(decimal lat,decimal longt, string rid){
        system.debug('--------Rid------->'+rid);
        system.debug('-----Lat-------->'+lat);
        system.debug('-----Long-------->'+longt);
        Task tsklist = [select Geolocation__Latitude__s, Geolocation__Longitude__s from Task where id = :rid];
        //Task tsk = new Task();
        tsklist.Geolocation__Latitude__s = lat;
        tsklist.Geolocation__Longitude__s = longt;
      //  tsklist.add(tsk);
        upsert tsklist;
        return tsklist;
    }
}