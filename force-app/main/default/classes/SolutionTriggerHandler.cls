public class SolutionTriggerHandler {
    public Static void doupdate(List<Solution> slist){
        List<Candidate__c> cval = new List<Candidate__c>();
        List<Candidate__c> clist = [Select id,first_name__C, SolutionName__c from Candidate__c];
        for(Solution sl : slist){
          //  system.debug('------snames----->'+sl.SolutionName);
            for(Candidate__c Cl :clist){
               // system.debug('------Cnames----->'+Cl.First_Name__c);
                if(sl.SolutionName == cl.first_name__C)
                    
                system.debug('------Before === snames----->'+sl.SolutionName);
                system.debug('------Before === Cnames----->'+Cl.First_Name__c);
                
                cl.SolutionName__c = sl.Id;
                
                system.debug('------After === snames----->'+sl.SolutionName);
                system.debug('------After === Cnames----->'+Cl.First_Name__c);
                
                 cval.add(cl);
               // system.debug('-----SolutionName---->'+cl.SolutionName__c);
            }
        } update cval;        
    }
}