public class SortbyDate {
    public List<Object_A__c> alist {set;get;}
    public Integer selectedyear {set;get;}
    
        public List<SelectOption> getYears(){
        List<Selectoption> options=new List<Selectoption>();
        for (Integer i = System.Today().year() - 3; i <= System.Today().year() ; i++) {
            options.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
        }
        return options;
    }
    
    public void submit()
    {
        string query='select Date__C from Object_A__c where CALENDAR_YEAR(Date__c) =: selectedyear order by date__c ASC';
        alist=database.query(query);
    }
}