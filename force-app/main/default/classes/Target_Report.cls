public class Target_Report {
    
    public List<Product_Detail__c> plist {set;get;}
    public List<String> mnth             {set;get;}
    public List<Selectoption> option     {set;get;}
    public Integer coun                  {set;get;}
    public Integer currentyear           {set;get;}
    public Integer prevyear              {set;get;}
    public Integer months                {set;get;}
    public Integer years                 {set;get;}
    public Boolean flag                  {set;get;}
  
  public Target_Report()
  {
    flag=false;
    coun=0;
    currentyear=Date.today().year();
    prevyear=currentyear-1;
    mnth=new List<String>{'Jan','Feb','Mar','Apr','May','Jun','July','Aug','Sep','Oct','Nov','Dec'};
    option=new List<Selectoption>();
    selectoption n=new selectoption('-None-', 'None');
    option.add(n);
    for(String s:mnth)
    {
    coun++;
    option.add(new selectoption(string.valueof(coun),s));
    }
  
  }
  
  public void submit()
  {
  flag=true;
  plist=[select id,name,(select id,TargetAmount__c from Targets__r where CALENDAR_MONTH(Date__c)=:months AND CALENDAR_year(Date__c)=:years),(select id,Achieved_Amount__c from Achives__r where CALENDAR_MONTH(Date__c)=:months AND CALENDAR_year(Date__c)=:years) from Product_Detail__c];
    
    }
}