@isTest
public class Target_Report_test {
	public static testmethod void testtarget()
    {
        Product_Detail__c p=new Product_Detail__c();
        p.Name='Test';
        insert p;
        
        Target__c t=new Target__c();
        t.TargetAmount__c=1000;
        t.Product_Name__c=p.Id;
        insert t;
        
        Achived__c a=new Achived__c();
        a.Achieved_Amount__c=800;
        a.Product_Name__c=p.Id;
        insert a;
        
        Target_Report tar=new Target_Report();
        tar.submit();
    }
}