public class TotalRecordCount 
{
    public Map<String, Map<Date, Integer>> mstmap  {set;get;}
    Public Integer count {set;get;}
    
    public TotalRecordCount()
    {
    mstmap=new Map<String, Map<Date,Integer>>();
    count=0;
    
    For(Child__c c:[select id,open_Date__c,status__C from child__C order by open_Date__c ASC])
    {
        if(!mstmap.containskey(c.status__c))
        mstmap.put(c.status__C, new Map<Date, Integer>());
       
       if(mstmap.containskey(c.status__c)) 
       if(!mstmap.get(c.status__C).containskey(c.open_Date__C))
       mstmap.get(c.status__c).put(c.open_date__C, count);
       if(mstmap.get(c.status__C).containskey(c.open_Date__C))
       mstmap.get(c.status__c).put(c.open_Date__C, mstmap.get(c.status__C).get(c.open_date__C)+1);
    }
    }

}