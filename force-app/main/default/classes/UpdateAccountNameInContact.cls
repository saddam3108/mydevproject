public class UpdateAccountNameInContact {
    Public static void doupdate(List<Contact> newcontact){
        List<Contact> con =new List<Contact>();
        List<String> phn = new List<String>();
        for(Contact c:newcontact){
            phn.add(c.Phone);
        }
        List<Account> acc = [Select id,name, phone from Account where phone In : phn];
        system.debug('---Account name---'+ acc +'----Size--------'+acc.size());
        if(acc.size() == 1){
            for(Account a : acc){
                system.debug('------Account phone---'+a.Phone);
                for(Contact co : newcontact){
                    system.debug('----Contact phone--'+co.Phone);
                    if(co.phone == a.phone){
                        co.AccountId = a.id;
                        con.add(co);
                    }
                }
            }
        } 
        else  {
            for(Account a : acc){
                system.debug('------Account phone---'+a.Phone);
                for(Contact co : newcontact){
                    system.debug('----Contact phone--'+co.Phone);
                    if(co.phone == a.phone){
                        co.AccountId = Null;
                        con.add(co);
                    }
                }
            }
        }
    }
    
    public static void doupdate2(List<Contact> con){
      Map<Id,string> conmap = new Map<Id,String>();
      for(Contact c:con){
      conmap.put(c.accountid,c.phone);
      }
      
      List<Account> acc = [select id,phone from Account where Id In:conmap.keyset()];
      for(Account a :acc){
              a.phone = conmap.get(a.id);
          }
          update acc;
       }
}