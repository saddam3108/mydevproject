public class paginationcontroller 
{    
    private integer totalRecs = 0;     
    private integer index = 0;
    private integer blockSize = 5;
    
    public string aname{get;set;}
    
    public List<Account> selectedAccounts{get;set;}
    public List<wrapperclass> lstwrp{get;set;}
    Public List<wrapperclass> searchlist {set;get;}           
    
    public paginationcontroller()
    {
        totalRecs = [select count() from Account];        
    }    
    
    public List<wrapperclass> getwrapaccs() 
    {
        lstwrp=new List<wrapperclass>();
        List<Account> Accs = Database.Query('SELECT Name,Industry FROM Account LIMIT :blockSize OFFSET :index');
        
        for(account a:Accs){
            wrapperclass wrp=new wrapperclass(a);
            lstwrp.add(wrp);
        }
        
        return lstwrp;
    }
    
    public PageReference processSelected() {
        selectedAccounts = new List<Account>();
        for (wrapperclass w : lstwrp) {
            if (w.selected == true) {
                selectedAccounts.add(w.acc);
            }
        }
        PageReference pdfPage = new PageReference('/apex/DisplayAccountListView_1_Page');
        return pdfPage;
    }
    
    public void doSearch(){
        searchlist=new List<wrapperclass>();
        system.debug('-------------wrap list in search button---------'+lstwrp);
        for(wrapperclass ac : lstwrp){
        if(ac.acc.Name.containsIgnoreCase(aname)){
            system.debug('---------aname----------'+aname);
            system.debug('----account details------'+ac);
            searchlist.add(ac);
            system.debug('----searchlist------'+searchlist);
            }
        } 
        lstwrp.clear();
        lstwrp = searchlist.clone();
    }      
    
    public void beginning()
    {
        index = 0;
    }
    
    public void previous()
    {
        index = index - blockSize;
    }
    
    public void next()
    {
        index = index + blockSize;
    }

    public void end()
    {
        index = totalrecs - math.mod(totalRecs,blockSize);
    }        
    
    public boolean getprev()
    {
        if(index == 0)
        return true;
        else
        return false;
    }  
    
    public boolean getnxt()
    {
        if((index + blockSize) > totalRecs)
        return true;
        else
        return false;
    } 
    
    public class wrapperclass{
        public Account acc{get;set;}
        public boolean selected {get;set;}
        
        public wrapperclass(Account a){
            acc=a;
            selected=false;
        }
    }        
}