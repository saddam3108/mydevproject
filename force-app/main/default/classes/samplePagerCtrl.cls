public class samplePagerCtrl {
    @AuraEnabled
    public static AccountPagerWrapper fetchAccount(Decimal pagenumber, Integer recordTodisplay){
        Integer pagesize = recordTodisplay;
        Integer offset = ((Integer)pagenumber-1)*pagesize;
        AccountPagerWrapper obj = new AccountPagerWrapper();
        obj.pagesize = pagesize;
        obj.page = (Integer)pagenumber;
        obj.total = [select count() from Account];
        obj.accounts = [select id,name,phone from account ORDER BY Name LIMIT :recordTodisplay OFFSET :offset];
        return obj;
    }
    public class AccountPagerWrapper{
        @AuraEnabled public Integer pagesize {set;get;}
        @AuraEnabled public Integer page {set;get;}
        @AuraEnabled public Integer total {set;get;}
        @AuraEnabled public List<Account> accounts {set;get;}
    }
}