public class searchAccountController {
@AuraEnabled
    public static List <Account> fetchAccount(string searchkeyword){
        string searchkey = searchkeyword+'%';
        List<Account> returnacc = new List<Account>();
        list<Account> lstofaccounts = [Select id,Name, phone, industry,Type,Fax from account where name like :searchkey];
        for(Account acc : lstofaccounts){
            returnacc.add(acc);
        }
        return returnacc;
    }
}