trigger CandidateTrigger on Candidate__c (After Insert, before update) {
    if(Trigger.isInsert && Trigger.isAfter){
    CandidateTriggerHandler.doSave(Trigger.new);
    }
   if(Trigger.isUpdate && Trigger.isbefore){
        CandidateTriggerHandler.doUpdate(Trigger.new);
    }
}